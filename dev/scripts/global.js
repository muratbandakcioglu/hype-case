
const HYP = {
    init: () => {
        HYP.test(HYP.globalOptions.testText);
        HYP.heroBannerTab();
        HYP.heroBanner();
    },
    globalOptions:  {
        testText: "Dünya"
    },
    heroBannerTab: () => {
		$('#hero-banner-tabs a').click(function (e) { 
			e.preventDefault();
			$('#hero-banner-tabs a').removeClass("active");
			$(this).addClass("active");
			$('.hero-banner').addClass('d-hidden')
			$($(this).data("target")).removeClass("d-hidden");
		});
    },
    heroBanner: () => {
        $(".hero-banner").slick({
			infinite: false,
			prevArrow: '<a class="slick-prev slick-button"></a>',
			nextArrow: '<a class="slick-next slick-button"></a>'
		});
		$(window).resize(function(index){
			$('.hero-banner')[0].slick.refresh();
			$('.hero-banner')[1].slick.refresh();
		});
    },
    test: think => {        
        console.log(`Hello World, hello ${think}`);
    }
}
$(document).ready(function () {
	HYP.init();
});

