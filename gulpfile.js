const gulp = require('gulp'),
	//jade = require('gulp-jade');
	pug = require('gulp-pug')
	sass = require('gulp-sass'),
	autoprefixer = require('gulp-autoprefixer'),//Prefix CSS with Autoprefixer
	cleanCSS = require('gulp-clean-css'),
	purge = require('gulp-css-purge'),//Purges duplicate CSS rules and more
	babel = require('gulp-babel'),
	terser = require('gulp-terser'), // minify js
	concat = require('gulp-concat'),
	//cache = require('gulp-cache'),//A temp file based caching proxy task for gulp.
	imagemin = require('gulp-imagemin'),
	rename = require('gulp-rename'),
	notify = require('gulp-notify'),//notification plugin for gulp
	del = require('del'),
	projectFile = "dist/assets/";


gulp.task('pug', () => {
	return gulp.src('dev/pug/*.pug')
		.pipe(pug({
			pretty: '	'
		})) // pip to pug plugin
		.pipe(gulp.dest('./dist'))// tell gulp our output folder
		.pipe(notify({ message: 'Jade task complete' }));
});

gulp.task('styles', () => {
	return gulp.src([
		//'dev/styles/vendor/animate.scss',
		'dev/styles/screen.scss'
		])
 		//.pipe(sourcemaps.init())
		.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
		.pipe(purge())
		.pipe(concat('screen.css'))
		.pipe(autoprefixer('last 2 version'))
		.pipe(cleanCSS())
		//.pipe(sourcemaps.write()
		.pipe(gulp.dest(projectFile + 'styles'))
		.pipe(notify({ message: 'Styles task complete' }));
});

gulp.task('scripts', () => {
	return gulp.src([
		'./node_modules/jquery/dist/jquery.min.js',
		'./node_modules/slick-carousel/slick/slick.min.js',
		// 'bower_components/jquery-validation/dist/jquery.validate.min.js',
		'dev/scripts/**/*.js'
		//'dev/scripts/global.js'
		])
		.pipe(babel({
			presets: ['@babel/env'],
			minified: true,
			comments: false
        }))
		.pipe(concat('global.js'))
		//.pipe(gulp.dest('i/assets/scripts/scripts'))
		//.pipe(rename({suffix: '.min'}))
		//.pipe(terser())
		.pipe(gulp.dest(projectFile + 'scripts'))
		.pipe(notify({ message: 'Scripts task complete' }));
});

gulp.task('images', () => {
  	return gulp.src('dev/images/**/*')
		.pipe(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true }))
		.pipe(gulp.dest(projectFile + 'images'))
		.pipe(notify({ message: 'Images task complete' }));
});

gulp.task('clean', () => {
    return del([projectFile + 'styles', projectFile + 'scripts', projectFile + 'images']);
});
/*
gulp.task('default', ['clean'], function() {
    gulp.start('styles', 'scripts', 'images');
});
*/

gulp.task('watch', () => {
	
	// Watch .scss files
	gulp.watch('./dev/styles/**/*.scss', gulp.series('styles'));
	
	// Watch .js files
	gulp.watch('./dev/scripts/**/*.js', gulp.series('scripts'));
	
	// Watch image files
	gulp.watch('./dev/images/**/*', gulp.series('images'));
	
	gulp.watch('./dev/pug/**/*', gulp.series('pug'));
	
	//console.log("görsel güncellendi")
	//livereload.listen();

	// Watch any files in dist/, reload on change
	//gulp.watch(['i/assets/**']).on('change', livereload.changed);

});
